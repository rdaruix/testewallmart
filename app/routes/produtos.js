module.exports = function(app){
    var controller = app.controllers.produtos;

    app.get('/produtos', controller.listaProdutos);
}
