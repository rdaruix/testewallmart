angular.module('Main').controller('ProdutosController', function($scope, $resource, $routeParams,$rootScope){
    $scope.listaProdutos = {};
    $scope.filtro = '';
    $scope.p = {
        installments:{}
    }
    $scope.produto = {};
    var produtosCarrinho = [];
    $rootScope.produtosCarrinho = [];
    
    
    var Produtos = $resource('/produtos', {'query':{method:'GET', isArray:false}});
	$scope.listaProdutos = Produtos.query(function(produtos){
        $scope.listaProdutos = produtos;
        angular.forEach(produtos, function(p, index){
            p.installments = p.installments[p.installments.length - 1];
        });
    });

    $rootScope.quantidadeCarrinho = 0;
    $rootScope.addCarrinho = function addCarrinho(e){
        console.log($rootScope.produtosCarrinho)
        
        var produtoNome = $(e.target).attr("data-nomeProduto");
        var produtoImg = $(e.target).attr("data-imgproduto");
        var produtoId = $(e.target).attr("data-idProduto");
        var strNome = produtoNome.substring(0,20) + ' ...';

        $scope.produto = {
            idProduto:produtoId,
            nomeProduto:strNome,
            imgProduto:produtoImg,
            qtdProduto: 1
        }

        if(produtosCarrinho <= []){
            produtosCarrinho.push($scope.produto);
            $rootScope.produtosCarrinho.push($scope.produto);
        }else if(produtosCarrinho > []){
            angular.forEach($rootScope.produtosCarrinho, function(prodCart, index){
                if(prodCart.idProduto === $scope.produto.idProduto){
                    prodCart.qtdProduto = parseInt(prodCart.qtdProduto) + 1
                }else{
                    if(_.findWhere($rootScope.produtosCarrinho, {idProduto: $scope.produto.idProduto}) == null){
                        $rootScope.produtosCarrinho.push($scope.produto);
                    }               
                }
            });
        }
        $rootScope.quantidadeCarrinho = $rootScope.produtosCarrinho.length;
    };
    
    $rootScope.removeDiv = function removeDiv(e) {
        
        var produto = $(e.target).attr("data-id");
        $("#"+produto).remove();
        
        $rootScope.quantidadeCarrinho = 0;
    }
    
    $rootScope.finalizarCompra = function finalizarCompra(){
        $rootScope.produtosCarrinho = [];
        $('#opacity').hide();
        $(".menuLateral").hide();
    }
});
