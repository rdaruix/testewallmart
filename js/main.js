angular.module('Main', ['ngRoute', 'ngResource']).config(function($routeProvider, $locationProvider){

    $routeProvider.when('/',{
        templateUrl:'/partials/produtos.html',
        controller:'ProdutosController'
    });


    $routeProvider.otherwise({redirectTo:'/'});

    $locationProvider.hashPrefix('');
});
